module Api

  class TweetAlreadyPosted < StandardError; end

  class TweetsController < ApplicationController

    rescue_from TweetAlreadyPosted, with: :render_unprocessable_entity
    rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity

    skip_before_action :verify_authenticity_token

    TWEETS_PER_PAGE = 50

    def create
      return head :bad_request if params[:body].blank? || params[:user_id].blank?

      raise TweetAlreadyPosted if previous_tweets.count > 0

      @tweet = Tweet.create!(
          user_id: params[:user_id],
          body: params[:body]
        )

      render json: @tweet
    end

    def index
      @tweets = Tweet.order("created_at ASC").limit(TWEETS_PER_PAGE).offset(offset)

      render json: @tweets
    end

    private

    def previous_tweets
      Tweet.where(user_id: params[:user_id], body: params[:body])
           .where('created_at >= :one_day_ago', one_day_ago: 24.hours.ago)
    end

    def render_unprocessable_entity
      head :unprocessable_entity
    end

    def page_params
      params.permit(:page).merge(per_page: TWEETS_PER_PAGE)
    end

    def page
      params[:page]
    end

    def offset
      return 0 if !page || page == 1

      page * (page.to_i - 1)
    end
  end
end
